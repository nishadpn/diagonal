package com.example.diagonal.handler;

import android.os.Bundle;
import android.text.TextWatcher;

public interface FragmentHandler {
    public void setTitle(String text);
    public void setSearchHandler(SearchHandler watcher);
    public void onPreviousState(Bundle instanceState);
}
