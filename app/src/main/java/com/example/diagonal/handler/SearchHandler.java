package com.example.diagonal.handler;

public interface SearchHandler {
    public void onSearchOpen();
    public void onTypeInSearchBox(String text,String previousText);
    public void onSearchClose();
}
