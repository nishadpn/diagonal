package com.example.diagonal.model;

import com.google.gson.annotations.SerializedName;

public class Poster {
    @SerializedName("poster-image")
    private String posterImage;
    @SerializedName("name")
    private String posterTitle;

    public Poster(){
    }
    public Poster(String posterTitle,String posterImage){
        this.posterTitle =posterTitle;
        this.posterImage =posterImage;
    }
    public String getPosterImage() {
        return posterImage;
    }

    public void setPosterImage(String posterImage) {
        this.posterImage = posterImage;
    }

    public String getPosterTitle() {
        return posterTitle;
    }

    public void setPosterTitle(String posterTitle) {
        this.posterTitle = posterTitle;
    }
}
