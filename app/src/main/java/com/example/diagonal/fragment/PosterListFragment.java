package com.example.diagonal.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.diagonal.R;
import com.example.diagonal.adapter.PosterListAdapter;
import com.example.diagonal.components.PosterItemDecoration;
import com.example.diagonal.handler.FragmentHandler;
import com.example.diagonal.handler.SearchHandler;
import com.example.diagonal.model.Poster;
import com.example.diagonal.util.ObjectFactory;
import com.example.diagonal.util.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PosterListFragment extends Fragment implements PosterListAdapter.OnBindListener, SearchHandler {
    private RecyclerView recyclerView;
    private FragmentHandler handler;
    private Bundle savedArgs;
    private int lastLoaded = 1;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
         return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.rvPosterList);
        initRecyclerView();
        handler.onPreviousState(savedArgs);
    }

    private void setFragmentData(){
        String json  = Utils.getJsonFromAssets(getContext(),"api/CONTENTLISTINGPAGE-PAGE1.json");
        try {
            JSONObject jsonObject = new JSONObject(json).getJSONObject("page");
            String pageTitle = jsonObject.getString("title");
            handler.setTitle(pageTitle);
            ArrayList<Poster> posters = new Gson().fromJson(jsonObject.getJSONObject("content-items").getString("content"),new TypeToken<ArrayList<Poster>>(){}.getType());
            setRecyclerView(posters);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initRecyclerView(){
        int columns  = 3;
        if(getContext().getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            columns  = 7;
        }
        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(),columns));
        recyclerView.addItemDecoration(new PosterItemDecoration(0,30,30,90,columns));
        setFragmentData();
    }

    public void setRecyclerView(ArrayList<Poster> posters){
        recyclerView.setAdapter(new PosterListAdapter(recyclerView.getContext(),posters,this));
    }


    public void setHandler(FragmentHandler handler){
        this.handler = handler;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        FragmentHandler handler = (FragmentHandler)context;
        setHandler(handler);
        handler.setSearchHandler(this);
        super.onAttach(context);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        savedArgs = savedInstanceState;
        if(savedArgs!=null){
            hasSearch = savedInstanceState.getBoolean(Utils.HAS_SEARCH);
        }
    }

    @Override
    public void onBind(int position, ArrayList values) {
        if(hasSearch){
            return;
        }
        int columns = ((GridLayoutManager)recyclerView.getLayoutManager()).getSpanCount();
        int loadIndex = (values.size()/20) +1;
        if(values.size()-position == 2*columns && lastLoaded !=loadIndex){
            Log.d("Divided",String.valueOf(loadIndex));
            String json  = Utils.getJsonFromAssets(getContext(),"api/CONTENTLISTINGPAGE-PAGE"+loadIndex+".json");
            if(json!=null){
                try {
                    JSONObject jsonObject = new JSONObject(json).getJSONObject("page");
                    String pageTitle = jsonObject.getString("title");
                    handler.setTitle(pageTitle);
                    ArrayList<Poster> posters = new Gson().fromJson(jsonObject.getJSONObject("content-items").getString("content"),new TypeToken<ArrayList<Poster>>(){}.getType());
                    PosterListAdapter adapter = (PosterListAdapter) recyclerView.getAdapter();
                    adapter.addAllElement(posters);
                    lastLoaded = loadIndex;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    private void searchFromJson(String text){
        String posterData = ObjectFactory.getInstance().getAppPreference(getContext()).getPosters();
        ArrayList<Poster> posters = new ArrayList<>();
        if(posterData.isEmpty()){
            int index = 1;
            String json = null;
            do {
                json  = Utils.getJsonFromAssets(getContext(),"api/CONTENTLISTINGPAGE-PAGE"+index+".json");
                if(json!=null){
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(json).getJSONObject("page");
                        ArrayList<Poster> posterExtracted = new Gson().fromJson(jsonObject.getJSONObject("content-items").getString("content"),new TypeToken<ArrayList<Poster>>(){}.getType());
                        if(posterExtracted != null){
                            posters.addAll(posterExtracted);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                index++;
            } while (json!=null);
        } else {
            posters = new Gson().fromJson(posterData,new TypeToken<ArrayList<Poster>>(){}.getType());
        }
        ArrayList<Poster> filteredPosters = new ArrayList<>();
        for (Poster poster:posters){
            if(poster.getPosterTitle().toLowerCase().contains(text.toLowerCase())){
                poster.setPosterTitle(poster.getPosterTitle().replaceAll("(?i)"+text,"<span style=\"color:#d8d911\">"+text+"</span>"));
                filteredPosters.add(poster);
            }
        }
        setRecyclerView(filteredPosters);
    }

    private boolean hasSearch  = false;
    @Override
    public void onSearchOpen() {

    }

    @Override
    public void onTypeInSearchBox(String text,String previousText) {
        if(text.length()>=3){
            hasSearch = true;
            searchFromJson(text);
        } else if(previousText.length()>=3) {
            setFragmentData();
        }
    }

    @Override
    public void onSearchClose() {
        if(hasSearch)
            setFragmentData();
        hasSearch = false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(Utils.HAS_SEARCH,hasSearch);
    }
}
