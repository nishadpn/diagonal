package com.example.diagonal;

import android.os.Bundle;

import com.example.diagonal.handler.SearchHandler;
import com.example.diagonal.handler.FragmentHandler;
import com.example.diagonal.util.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.transition.Fade;
import androidx.transition.Scene;
import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import android.os.PersistableBundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements FragmentHandler, View.OnClickListener {
    private TextView tvTitle;
    private ImageView imSearch;
    private View searchContainer;
    private SearchHandler handler;
    private ViewGroup toolBarMain;
    private ViewGroup searchTollBar;
    private EditText searchBox;
    private Button btSearchCancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);


    }

    public void setHandler(SearchHandler handler) {
        this.handler = handler;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews(){
        tvTitle = findViewById(R.id.tvTitleText);
        imSearch = findViewById(R.id.imSearch);
        toolBarMain = findViewById(R.id.tdMain);
        searchTollBar =  findViewById(R.id.tdSearch);
        searchBox = findViewById(R.id.etSearch);
        btSearchCancel = findViewById(R.id.btSearchCancel);
        imSearch.setOnClickListener(this);
        btSearchCancel.setOnClickListener(this);
        searchBox.addTextChangedListener(new TextWatcher() {
            private String previousText = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                previousText = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handler.onTypeInSearchBox(s.toString(),previousText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void showSearchBar(){
        toolBarMain.setVisibility(View.GONE);
        searchTollBar.setVisibility(View.VISIBLE);
        transition((ViewGroup) findViewById(R.id.toolbar),searchTollBar,Gravity.END);
        searchBox.requestFocus();
        Utils.openKeyPad(MainActivity.this,searchBox);
    }

    private void hideSearchBar(){
        searchTollBar.setVisibility(View.GONE);
        toolBarMain.setVisibility(View.VISIBLE);
        searchBox.setText("");
        transition((ViewGroup) findViewById(R.id.toolbar),toolBarMain,Gravity.END);
        Utils.hideKeyPad(MainActivity.this,searchBox);
    }

    @Override
    public void setTitle(String text) {
        tvTitle.setText(text);
    }

    @Override
    public void setSearchHandler(SearchHandler handler) {
      this.handler = handler;
    }

    @Override
    public void onPreviousState(Bundle instanceState) {
        if(instanceState!=null && instanceState.getBoolean(Utils.HAS_SEARCH)){
           showSearchBar();
        }
    }

    private void transition(ViewGroup sceneRoot,ViewGroup hierarchy,int slideEdge){
        sceneRoot.removeView(hierarchy);
        Scene scene = new Scene(sceneRoot,hierarchy);
        Transition fadeTransition = new Fade();
        Transition slideTransition = new Slide(slideEdge);
        TransitionManager.go(scene,slideTransition);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imSearch:
                showSearchBar();
                handler.onSearchOpen();
                break;
            case R.id.btSearchCancel:
                hideSearchBar();
                handler.onSearchClose();
                break;
            default:break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
}
