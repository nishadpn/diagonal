package com.example.diagonal.components;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PosterItemDecoration extends RecyclerView.ItemDecoration {
    private int top;
    private int left;
    private int right;
    private int bottom;
    private int columnSize;

    public PosterItemDecoration(int top, int left, int right, int bottom,int columnSize) {
        this.top = top;
        this.left = left;
        this.right = right;
        this.bottom = bottom;
        this.columnSize = columnSize;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.left = this.left;
        outRect.top = 0;
        outRect.right  = this.right;
        outRect.bottom  = this.bottom;
        if(parent.getChildLayoutPosition(view)/columnSize<=1){
            outRect.top = this.top;
        }
    }
}
