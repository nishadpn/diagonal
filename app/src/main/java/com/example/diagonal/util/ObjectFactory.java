package com.example.diagonal.util;

import android.content.Context;

public class ObjectFactory {
    private static ObjectFactory instance;
    private AppPreference appPreference;
    private ObjectFactory(){

    }
    public static ObjectFactory getInstance(){
        if(instance==null){
            instance = new ObjectFactory();
        }
        return instance;
    }

    public AppPreference getAppPreference(Context context) {
        if (appPreference==null)
            appPreference = new AppPreference(context);
        return appPreference;
    }
}
