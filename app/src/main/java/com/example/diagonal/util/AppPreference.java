package com.example.diagonal.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.diagonal.R;

import java.util.ArrayList;

public class AppPreference {
    public static final String POSTER_DATA="POSTER_DATA";
    private SharedPreferences mSharedPreferences;
    AppPreference(Context context){
        if(context!=null){
            mSharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name),Context.MODE_PRIVATE);
        }
    }

    public void savePosters(String posters){
        SharedPreferences.Editor editor =mSharedPreferences.edit();
        editor.putString(POSTER_DATA,posters);
    }

    public String getPosters(){
       return mSharedPreferences.getString(POSTER_DATA,"");
    }
}
