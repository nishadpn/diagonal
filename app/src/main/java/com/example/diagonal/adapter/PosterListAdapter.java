package com.example.diagonal.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.diagonal.R;
import com.example.diagonal.model.Poster;
import com.example.diagonal.util.Utils;

import java.util.ArrayList;

public class PosterListAdapter extends RecyclerView.Adapter<PosterListAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Poster> mValues;
    private OnBindListener onBindListener;

    public PosterListAdapter(Context context,ArrayList<Poster> mValues,@NonNull OnBindListener onBindListener) {
        this.mContext = context;
        this.mValues = mValues;
        this.onBindListener = onBindListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(position);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
    public void addAllElement(ArrayList<Poster> posters) {
        mValues.addAll(posters);
//        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imPoster);
            textView = itemView.findViewById(R.id.tvPoster);
        }
        public void bindData(int position){
            textView.setText(Html.fromHtml(mValues.get(position).getPosterTitle()));
//            textView.setText(mValues.get(position).getPosterTitle());
            Glide.with(mContext).load(Utils.getImageAsset(mContext,"images/"+mValues.get(position).getPosterImage())).fitCenter().placeholder(R.drawable.placeholder_for_missing_posters).into(imageView);
            onBindListener.onBind(position,mValues);
        }
    }
    public interface OnBindListener {
        void onBind(int position,ArrayList values);
    }
}
